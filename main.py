import os
import configparser

from components.server import TaskServer, DistributedTaskServer
from components.register import FuncRegister

# ====================================================================================================
# ---------------------------------------- Function Regisgter ----------------------------------------
# ====================================================================================================

from worker.simple import add
from worker.blockchain import *

def create_function_register(config):
    func_register = FuncRegister(config)
    func_register.register_func('simple.add', add)
    func_register.register_func('blockchain.add.track_trace', execute_push_track_trace_to_blockchain)
    func_register.register_func('blockchain.add.import', execute_push_import_to_blockchain)
    func_register.register_func('blockchain.add.product', execute_push_product_to_blockchain)
    func_register.register_func('blockchain.add.data', execute_add_data_to_blockchain)
    func_register.register_func('blockchain.get.credit', execute_get_owner_credit_point)

    return func_register

# ====================================================================================================
# --------------------------------------------- Main App ---------------------------------------------
# ====================================================================================================

if __name__ == '__main__':
    config = configparser.ConfigParser()
    config.read('app.conf')

    if not os.path.exists(config['worker']['tmp_folder']):
        os.mkdir(config['worker']['tmp_folder'])
    
    func_register = create_function_register(config)

    task_servers = DistributedTaskServer(config=config, func_register=func_register)
    task_servers.run()