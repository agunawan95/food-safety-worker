import random

from lib.emitter import Emitter

emit_list = []

for i in range(50):

    data = {
        'a': random.randint(1, 100),
        'b': random.randint(1, 100)
    }

    r = random.randint(1, 10)
    if r == 1:
        data['b'] = 'str'
    elif r == 2:
        del data['b']

    emitter = Emitter()
    emitter.emit('simple.add', data)
    emit_list.append(emitter)


while True:
    finish = True
    for emitter in emit_list:
        current = emitter.get_current_job_status()
        #print(current)
        if current['status'] == 'INITIALIZE':
            finish = False
    
    if finish:
        for emitter in emit_list:
            current = emitter.get_current_job_status()
            print(current)
        break

print('Cleanup ...')
import configparser
import redis

config = configparser.ConfigParser()
config.read('app.conf')

r = redis.Redis(host=config['redis']['host'], port=int(config['redis']['port']), db=int(config['redis']['db']))


for key in r.scan_iter("prefix:*"):
    r.delete(key)