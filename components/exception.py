from components.worker import update_task_status

# ==================================================================================================
# ---------------------------------------- System Exception ----------------------------------------
# ==================================================================================================

class WorkerException(Exception):
    def __init__(self, task_id, msg) -> None:
        self.msg = f"[{self.__class__.__name__}]: {msg}"
        self.task_id = task_id

        update_task_status(task_id=self.task_id, status='ERROR', msg=self.msg)
    
    def __str__(self) -> str:
        return self.msg

# ==================================================================================================
# ---------------------------------------- Custom Exception ----------------------------------------
# ==================================================================================================

class TypeMismatchException(WorkerException):
    
    def __init__(self, task_id, msg) -> None:
        WorkerException.__init__(self, task_id, msg)
    

class BadRequestException(Exception):
    
    def __init__(self, task_id, msg) -> None:
        WorkerException.__init__(self, task_id, msg)