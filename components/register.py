class FuncRegister:

    def __init__(self, config) -> None:
        self.func_list = {}
        self.config = config
    
    def register_func(self, func_tag, f):
        if func_tag not in  self.func_list:
            self.func_list[func_tag] = f
    
    def run(self, func_tag, data):
        return self.func_list[func_tag](data, self.config)