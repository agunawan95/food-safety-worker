import redis
import pickle
import configparser
import json

def update_task_status(task_id, status, result=None, msg=''):
    status_data = {
        'status': status,
        'result': result,
        'msg': msg
    }

    print(f"Task ID: {task_id} -> {status_data}")

    config = configparser.ConfigParser()
    config.read('app.conf')

    r = redis.Redis(host=config['redis']['host'], port=int(config['redis']['port']), db=int(config['redis']['db']))
    r.set(task_id, pickle.dumps(status_data))

def worker(f):
    def wrapper(*args, **kwargs):
        try:
            result = f(*args, **kwargs)
            data = args[0]
            task_id = data['header']['task_id']

            update_task_status(task_id=task_id, status='SUCCESS', result=result)
        except:
            pass
    
    return wrapper


def clean_output(output_str):
    print('Clean Output ...')
    lines = output_str.split("\n")
    target_data = []
    for line in lines:
        if str(line).strip().lower().startswith('query response:'):
            clean_str = line.replace('query response:', '').strip()
            target_data.append(json.loads(clean_str))
        elif str(line).strip().lower().startswith('insert response:'):
            clean_str = line.replace('insert response:', '').strip()
            target_data.append(json.loads(clean_str))
    return target_data