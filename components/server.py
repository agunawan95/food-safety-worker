import pika
import json
import uuid
import threading

class TaskServer(threading.Thread):

    def __init__(self, config, func_register) -> None:
        threading.Thread.__init__(self)

        self.server_id = str(uuid.uuid4())
        self.config = config
        self.prefetch_count = int(self.config['rabbitmq']['prefetch_count'])
        self.func_register = func_register

        self.initlialize_queue()

    def handle_task_request(self, ch, method, properties, body):
        print(f"[{self.server_id}] Handle Request: {body}")
        data = json.loads(body)
        func_tag = data['header']['func_tag']

        self.func_register.run(func_tag, data)

        ch.basic_ack(delivery_tag=method.delivery_tag)


    def initlialize_queue(self):
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(host=self.config['rabbitmq']['host']))
        self.channel = self.connection.channel()

        self.channel.queue_declare(queue=self.config['rabbitmq']['queue_name'], durable=True)

        self.channel.basic_qos(prefetch_count=self.prefetch_count)
        self.channel.basic_consume(queue=self.config['rabbitmq']['queue_name'], on_message_callback=self.handle_task_request)
    
    def run(self):
        self.channel.start_consuming()

class DistributedTaskServer:

    def __init__(self, config, func_register) -> None:
        self.config = config
        self.n_thread = int(self.config['server']['n_thread'])
        self.func_register = func_register
        self.running_server = []
    
    def run(self):
        for i in range(self.n_thread):
            print(f"Initialize {i+1}")
            task_server = TaskServer(config=self.config, func_register=self.func_register)
            task_server.start()
            self.running_server.append(task_server)
