from components.worker import worker
from components.exception import TypeMismatchException, BadRequestException

@worker
def add(data, config):
    task_id = data['header']['task_id']
    payload = data['payload']

    if ('a' not in payload) or ('b' not in payload):
        print('Bad Request')
        raise BadRequestException(task_id, 'Bad Request, attribute a & b must be present in Payload')

    if not (type(payload['a']) is int) or  not (type(payload['b']) is int):
        raise TypeMismatchException(task_id, 'Type must be an Integer')

    total = float(payload['a']) + float(payload['b'])
    return total