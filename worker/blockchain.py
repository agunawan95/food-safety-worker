import uuid
import os
import datetime

from components.worker import clean_output, worker

@worker
def execute_push_track_trace_to_blockchain(data, config):
    cmd = config['worker']['invoker']
    tmp_folder = config['worker']['tmp_folder']

    partial_key = str(uuid.uuid4())
    filename = f"{data['header']['filename'].replace('.csv', '')}_{partial_key}.csv"

    target_file = f"{tmp_folder}/{filename}"

    with open(target_file, 'w') as f:
        f.write(data['payload']['filedata'])
        f.close()
    
    args = f'-se {target_file}'

    output = os.popen(cmd + ' ' + args).read()
    data = clean_output(output)
    
    os.remove(tmp_folder + '/' + filename)

    return data

@worker
def execute_push_import_to_blockchain(data, config):
    cmd = config['worker']['invoker']
    tmp_folder = config['worker']['tmp_folder']

    partial_key = str(uuid.uuid4())
    filename = f"{data['header']['filename'].replace('.csv', '')}_{partial_key}.csv"

    target_file = f"{tmp_folder}/{filename}"

    with open(target_file, 'w') as f:
        f.write(data['payload']['filedata'])
        f.close()
    
    args = f'-si {target_file}'

    output = os.popen(cmd + ' ' + args).read()
    data = clean_output(output)
    
    os.remove(tmp_folder + '/' + filename)

    return data

@worker
def execute_push_product_to_blockchain(data, config):
    cmd = config['worker']['invoker']
    tmp_folder = config['worker']['tmp_folder']

    partial_key = str(uuid.uuid4())
    filename = f"{data['header']['filename'].replace('.json', '')}_{partial_key}.json"

    target_file = f"{tmp_folder}/{filename}"

    with open(target_file, 'w') as f:
        f.write(data['payload']['filedata'])
        f.close()
    
    args = f'-sp {target_file}'

    output = os.popen(cmd + ' ' + args).read()
    data = clean_output(output)
    
    os.remove(tmp_folder + '/' + filename)

    return data

# DEPRECATED! MIGHT REMOVE SOON!

@worker
def execute_add_data_to_blockchain(data, config):
    cmd = config['worker']['invoker']
    key = str(uuid.uuid4())

    final_data = {
        'previous_key': key,
        'new_key': key,
        'generator_gln': data['payload']['generator_gln'],
        'event_id': str(uuid.uuid4()),
        'event_type': str(data['payload']['event_type']),
        'input_gtin': str(data['payload']['input_gtin']),
        'output_gtin': str(data['payload']['output_gtin']),
        'serial_number': str(data['payload']['serial_number']),
        'event_time': '"{}"'.format(datetime.datetime.now().isoformat()),
        'event_location': str(data['payload']['longitude']) + ',' + str(data['payload']['latitude']),
        'location_name': str(data['payload']['location_name']),
        'company_name': str(data['payload']['company_name'])
    }

    # data['event_time'] = '"{}"'.format(data['event_time'])
    
    args = '-s {previous_key} {new_key} {generator_gln} {event_id} {event_type} {input_gtin} {output_gtin} {serial_number} {event_time} {event_location} {location_name} {company_name}'
    args = args.format(**final_data)
    
    output = os.popen(cmd + ' ' + args).read()
    data = clean_output(output)

    return data

# DEPRECATED! MIGHT REMOVE SOON!

@worker
def execute_get_owner_credit_point(data, config):
    cmd = config['worker']['invoker']
    args = f"-q {data['payload']['owner_id']}"

    output = os.popen(cmd + ' ' + args).read()
    data = clean_output(output)

    return data